import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // These are member variables for input and output data.
  height: number;
  weight: number;
  bmi: number;

  constructor() {
    // Give initial values for member variables. These are displayed
    // when app is opened.
    this.height = 0;
    this.weight = 0;
    this. bmi = 0;
  }

  calculate() {
    // Calculate bmi using data in member variables.
    this.bmi = this.weight / (Math.pow(this.height, 2));
  }
}
